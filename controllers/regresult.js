const regresult = require('../db_apis/regresult.js');

async function get(req, res, next) {
  try {

    if (req.query.buildName && req.query.buildId) {
      const context = {};
      context.buildName = req.query.buildName;
      context.buildId = parseInt(req.query.buildId, 10);

      const rows = await regresult.find(context);

      if (rows.length === 1) {
        res.status(200).json(rows[0]);

      } else if (rows.length > 1) {
        res.status(200).json(rows);
      }
      else {
        res.status(404).end();
      }
    }
    else{
      var reply = {};
      reply.reason = 'Both buildName and buildId query parameters are required'
      res.status(400).json(reply);
    }
  } catch (err) {
    next(err);
  }
}

module.exports.get = get;