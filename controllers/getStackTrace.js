const getStackTrace = require('../db_apis/getStackTrace.js');

async function get(req, res, next) {
  try {

    if (req.params.id) {
      const context = {};
      context.id = req.params.id;

      const rows = await getStackTrace.find(context);

      if (rows) {
        res.status(200).json(rows[0]);

      } 
      else {
        res.status(404).end();
      }
    }
    else{
      var reply = {};
      reply.reason = 'Test ID is missing in the request.'
      res.status(400).json(reply);
    }
  } catch (err) {
    next(err);
  }
}

module.exports.get = get;