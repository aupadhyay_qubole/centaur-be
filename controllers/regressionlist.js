const regressionlist = require('../db_apis/regressionlist.js');
 
async function get(req, res, next) {
  try {
 
    const rows = await regressionlist.find();
 
    if (rows.length ===1) {
      
        res.status(200).json(rows[0]);
       
    } else if (rows.length > 1){
      res.status(200).json(rows);
    }
    else {
        res.status(404).end();
    }
  } catch (err) {
    next(err);
  }
}
 
module.exports.get = get;