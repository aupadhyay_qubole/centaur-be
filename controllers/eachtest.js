const regresult = require('../db_apis/eachtest.js');

async function get(req, res, next) {
  try {

    if (req.query.testId) {
      const context = {};
      context.testId = parseInt(req.query.testId, 10);
      const rows = await regresult.find(context);

      if (rows.length === 1) {
        res.status(200).json(rows[0]);

      } else if (rows.length > 1) {
        res.status(200).json(rows);
      }
      else {
        res.status(404).end();
      }
    }
    else {
      var reply = {};
      reply.reason = 'testId query parameters is required'
      res.status(400).json(reply);
    }
  } catch (err) {
    next(err);
  }
}

async function put(req, res, next) {
  try {
    console.log("req.body.testId = " + req.body.testId);
    if (req.body.testId) {
      const context = {};
      context.comment = req.body.comment;
      context.issue = req.body.issue;
      context.bugId = req.body.bugId;

      let regex = /[0-9]+(,[0-9]+)*/g
      if (regex.test(req.body.testId)) {
        context.testId = req.body.testId;
      }
      else {
        reply.reason = 'testId query parameters are required'
        res.status(400).json(reply);
      }
      const rows = await regresult.update(context);
      console.log("rows === " + rows);
      res.status(200).json(rows);
    }
    else {
      var reply = {};
      reply.reason = 'testId query parameters is required'
      res.status(400).json(reply);
    }
  } catch (err) {
    var reply = {};
    reply.reason = 'Invalid Request'
    res.status(400).json(reply);
  }
}
module.exports.get = get;
module.exports.put = put;
