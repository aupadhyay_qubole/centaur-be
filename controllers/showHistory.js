const getVerticalTrends = require('../db_apis/showHistory.js');

async function get(req, res, next) {
  try {

    if (req.query.testName && req.query.testsParam && req.query.parentJob) {
      const context = {};
      context.testName = decodeURIComponent(req.query.testName);
      context.testsParam = decodeURIComponent(req.query.testsParam);
      context.jobName = decodeURIComponent(req.query.jobName);
      context.parentJob = decodeURIComponent(req.query.parentJob);

      const rows = await getVerticalTrends.find(context);

      if (rows) {
        res.status(200).json(rows);

      } 
      else {
        res.status(404).end();
      }
    }
    else{
      var reply = {};
      reply.reason = 'testName, testsParam and parentJob query parameters are required'
      res.status(400).json(reply);
    }
  } catch (err) {
    next(err);
  }
}

module.exports.get = get;