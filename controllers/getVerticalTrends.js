const getVerticalTrends = require('../db_apis/getVerticalTrends.js');

async function get(req, res, next) {
  try {

    if (req.query.buildName && req.query.vertical) {
      const context = {};
      context.buildName = req.query.buildName;
      context.vertical = req.query.vertical;

      const rows = await getVerticalTrends.find(context);

      if (rows) {
        res.status(200).json(rows);

      } 
      else {
        res.status(404).end();
      }
    }
    else{
      var reply = {};
      reply.reason = 'Both buildName and vertical query parameters are required'
      res.status(400).json(reply);
    }
  } catch (err) {
    next(err);
  }
}

module.exports.get = get;