const express = require('express');
const router = new express.Router();
const regresult = require('../controllers/regresult.js');
const regressionlist = require('../controllers/regressionlist.js');
const verticaleachtest = require('../controllers/verticaleachtest.js');
const eachtest = require('../controllers/eachtest.js');
const getVerticalTrends = require('../controllers/getVerticalTrends.js');
const showHistory = require('../controllers/showHistory.js');
const getStackTrace = require('../controllers/getStackTrace');

const apiVersion = 'v1';

router.route('/' + apiVersion + '/regresult/:id?')
    .get(regresult.get);

router.route('/' + apiVersion + '/regressionlist')
    .get(regressionlist.get);

router.route('/' + apiVersion + '/verticaleachtest')
    .get(verticaleachtest.get);

router.route('/' + apiVersion + '/eachtest')
    .get(eachtest.get);

router.route('/' + apiVersion + '/eachtest')
    .put(eachtest.put);

router.route('/' + apiVersion + '/getVerticalTrends')
    .get(getVerticalTrends.get);

router.route('/' + apiVersion + '/showHistory')
    .get(showHistory.get);

router.route('/' + apiVersion + '/getStackTrace/:id?')
    .get(getStackTrace.get);

module.exports = router;