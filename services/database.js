var mysql = require('mysql');

const dbConfig = require('../config/database.js');

var pool;

async function initialize() {
    pool = await mysql.createPool(dbConfig.config);
}

module.exports.initialize = initialize;

async function close() {
    await connection.end();
}

module.exports.close = close;

function simpleExecute(statement, values = []) {
    return new Promise(async (resolve, reject) => {

        // opts.outFormat = oracledb.OBJECT;
        // opts.autoCommit = true;

        try {
            pool.getConnection(async function (err, connection) {
                if (err) throw err; // not connected!

                // Use the connection
                var result = await connection.query(statement, values, function (error, results, fields) {
                    if (error) throw error;
                    console.log(result.sql);
                    var res = JSON.stringify(results);
                    console.log(res);
                    connection.release();
                    resolve(results);
                });
            });



        } catch (err) {
            reject(err);
        } finally {
            // if (connection) { // conn assignment worked, need to close
            //     try {
            //         await connection.end();
            //     } catch (err) {
            //         console.log(err);
            //     }
            // }
        }
    });
}

module.exports.simpleExecute = simpleExecute;