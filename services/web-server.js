const http = require('http');
const express = require('express');
var cors = require('cors');
const webServerConfig = require('../config/web-server.js');
const morgan = require('morgan');
const database = require('./database.js');
const router = require('./router.js');
const bodyParser = require('body-parser');
 
let httpServer;
 
function initialize() {
  return new Promise((resolve, reject) => {
    const app = express();
    httpServer = http.createServer(app);

    app.use(morgan('combined'));
    
    app.use(cors());
    app.use(bodyParser.json());
    app.use('/api', router);
 
    httpServer.listen(webServerConfig.port)
      .on('listening', () => {
        console.log(`Web server listening on localhost:${webServerConfig.port}`);
 
        resolve();
      })
      .on('error', err => {
        reject(err);
      });
  });
}
 
module.exports.initialize = initialize;

function close() {
    return new Promise((resolve, reject) => {
      httpServer.close((err) => {
        if (err) {
          reject(err);
          return;
        }
   
        resolve();
      });
    });
  }
   
  module.exports.close = close;