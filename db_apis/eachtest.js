const database = require('../services/database.js');

const updateBaseQuery = `update test_execution set `
const getBaseQuery = ` select * from test_execution `

async function find(context) {
  let query = getBaseQuery;
  var values = [];

  if (context.testId) {
    values.push(context.testId);
    query += ` where id in (?) `;
  }

  const result = await database.simpleExecute(query, values);
  console.log(result)
  return result;
}

async function update(context) {
  let query = updateBaseQuery;
  var values = [];

  console.log("context.type = " + context.bugId + "  context.message =" + context.comment + " context.testId=" + context.testId)
  if ((context.type || context.comment) && context.testId) {
    if (context.bugId != null) {
      values.push(context.bugId);
      query += " Bug_Id = ? ,"
    }
    if (context.comment != null) {
      values.push(context.comment);
      values.push(context.issue);
      query += " comment = ? , bug_reason= ? "
    }

    values.push(context.testId);
    query += ` where id in (?) `;
  }
  else {
    const res = [];
    return res
  }
  console.log(" query = " + query)

  const result = await database.simpleExecute(query, values);
  console.log(result)
  return result;
}

module.exports.find = find;
module.exports.update = update;
