const database = require('../services/database.js');

const baseQuery = `Select * from (SELECT parent_job_build_id as regression_job_id, super_parent_job as regression_job, run_date as date, vertical, build_id, SUM(CASE WHEN status in ('PASSED','FIXED') THEN 1 ELSE 0 END) as passed,SUM(CASE WHEN status in ('FAILED','REGRESSION') THEN 1 ELSE 0 END) as failed,SUM(CASE WHEN status = 'SKIPPED' THEN 1 ELSE 0 END) as skipped, COUNT(*) as total, SUM(CASE WHEN bug_reason = 'TEST' THEN 1 ELSE 0 END) as test_issues, SUM(CASE WHEN bug_reason = 'ENV' THEN 1 ELSE 0 END) as env_issues, SUM(CASE WHEN bug_reason = 'DATA' THEN 1 ELSE 0 END) as data_issues, SUM(CASE WHEN Bug_id IS NOT NULL THEN 1 ELSE 0 END) as bug_reported, SUM(CASE WHEN status in ('FAILED','REGRESSION') and Bug_id IS NULL and bug_reason IS NULL THEN 1 ELSE 0 END) as analysis FROM qubole_qa_dashboard.test_execution Group BY vertical, regression_job_id , regression_job) as countTable`;

async function find(context) {
  let query = baseQuery;
  var values = [];

  if (context.buildName) {
    values.push(context.buildName);

    query += ` where regression_job = ?`;
  }

  if (context.buildId) {
    values.push(context.buildId);

    query += ` and regression_job_id = ?`;
  }

  query += ` order by regression_job_id ASC, vertical ASC`;

  const result = await database.simpleExecute(query, values);

  const res = [];


  var resObject = {};
  var vertical_counts = [];
  for (i = 0; i < result.length; i++) {

    if (i === 0) {
      resObject.regression_job_id = result[i].regression_job_id;
      resObject.regression_job = result[i].regression_job;
      resObject.date = result[i].date;
    }
    if (i > 0) {
      if (resObject.regression_job_id) {
        if (resObject.regression_job_id != result[i].regression_job_id) {
          resObject.details = vertical_counts;
          res.push(resObject);
          resObject = {};
          vertical_counts = [];
          resObject.regression_job_id = result[i].regression_job_id;
          resObject.regression_job = result[i].regression_job;
          resObject.date = result[i].date;
        }
      }
    }

    vertical_counts.push({
      vertical: result[i].vertical,
      build_id: result[i].build_id,
      passed: result[i].passed,
      failed: result[i].failed,
      skipped: result[i].skipped,
      total: result[i].total,
      count_test_issues: result[i].test_issues,
      count_env_issues: result[i].env_issues,
      count_data_issues: result[i].data_issues,
      bugs_attached: result[i].bug_reported,
      analysis : result[i].analysis
    });

    if (i == (result.length - 1)) {
      resObject.details = vertical_counts;
      res.push(resObject);
    }

  }
  console.log(res);
  return res;
}

module.exports.find = find;