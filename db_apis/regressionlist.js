const database = require('../services/database.js');

const baseQuery = `Select super_parent_job as regression_job, parent_job_build_id as jenkins_build_id, count(*) as total_tests, SUM(CASE WHEN status = 'FAILED' THEN 1 ELSE 0 END) as failed, SUM(CASE WHEN status = 'REGRESSION' THEN 1 ELSE 0 END) as regressions, concat(round(( SUM(CASE WHEN status in ('PASSED','FIXED') THEN 1 ELSE 0 END)/count(*)  * 100 ),2),'%') AS pass_percentage, run_date, DATE_FORMAT(run_date, '%d-%M-%y') as regression_date from qubole_qa_dashboard.test_execution group by regression_job, jenkins_build_id order by run_date DESC`;

async function find() {
  let query = baseQuery;
  var values = [];

  const result = await database.simpleExecute(query, values);

  return result;
  
}

module.exports.find = find;