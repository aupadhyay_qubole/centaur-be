const database = require('../services/database.js');

const baseQuery = `select id,job_name,build_id,name_of_test as name,Bug_id,bug_reason,comment,param_of_test as param, class_name, status from test_execution`

async function find(context) {
  let query = baseQuery;
  var values = [];

  if (context.buildName) {
    values.push(context.buildName);

    query += ` where super_parent_job = ?`;
  }

  if (context.buildId) {
    values.push(context.buildId);

    query += ` and parent_job_build_id = ?`;
  }

  if (context.vertical) {
    values.push(context.vertical);

    query += ` and vertical = ?`;
  }

  const result = await database.simpleExecute(query, values);
  console.log(result)
  return result;
}

module.exports.find = find;