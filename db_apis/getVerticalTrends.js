const database = require('../services/database.js');

const baseQuery = `Select regression_job, date, vertical,build_id, concat(round(( passed/total * 100 ),2),'%') AS pass_percentage from (SELECT parent_job_build_id as regression_job_id, super_parent_job as regression_job, DATE_FORMAT(run_date, '%d-%M-%y') as date, vertical, build_id, SUM(CASE WHEN status in ('PASSED','FIXED') THEN 1 ELSE 0 END) as passed, COUNT(*) as total FROM qubole_qa_dashboard.test_execution Group BY vertical, regression_job, regression_job_id)  as countTable`;

async function find(context) {
  let query = baseQuery;
  var values = [];

  if (context.buildName) {
    values.push(context.buildName);

    query += ` where regression_job = ?`;
  }

  if (context.vertical) {
    values.push(context.vertical);

    query += ` and vertical = ?`;
  }

  query += ` order by date ASC`;

  const result = await database.simpleExecute(query, values);

  var resObject = {};
  var vertical_trends = [];
  for (i = 0; i < result.length; i++) {

    if (i === 0) {
      resObject.regression_job = result[i].regression_job;
      resObject.vertical = result[i].date;
    }

    // if (i > 0) {
    //   if (resObject.regression_job_id) {
    //     if (resObject.regression_job_id != result[i].regression_job_id || (i === result.length - 1)) {
    //       resObject.trend_details = vertical_trends;
    //       res.push(resObject);
    //       resObject = {};
    //       vertical_counts = [];
    //       resObject.regression_job_id = result[i].regression_job_id;
    //       resObject.regression_job = result[i].regression_job;
    //       resObject.date = result[i].date;
    //     }
    //   }
    // }

    vertical_trends.push({
      date: result[i].date,
      build_id: result[i].build_id,
      pass_percentage: result[i].pass_percentage,
    });

    resObject.trend_details = vertical_trends;


  }

  console.log(resObject);
  return resObject;
}

module.exports.find = find;