const database = require('../services/database.js');

const baseQuery = 'Select id, errorStackTrace, errorDetails from qubole_qa_dashboard.test_execution';

async function find(context) {
    let query = baseQuery;
    var values = [];

    if (context.id) {
        values.push(context.id);

        query += ' where id = ?';
    }

    const result = await database.simpleExecute(query, values);

    return result;

}

module.exports.find = find;