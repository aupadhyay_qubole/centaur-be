const database = require('../services/database.js');

const baseQuery = `Select super_parent_job, job_name, name_of_test, status, DATE_FORMAT(run_date, '%d-%M-%y') as rdate, Bug_Id, bug_reason, comment from qubole_qa_dashboard.test_execution`;

async function find(context) {
    let query = baseQuery;
    var values = [];

    if (context.testName) {
        values.push(context.testName);

        query += ` where name_of_test = ?`;
    }

    if (context.testsParam) {
        if (context.testsParam != 'null') {
            values.push(context.testsParam);
            query += ` and param_of_test = ?`;
        }

    }

    if (context.jobName) {
        values.push(context.jobName + '%');

        query += ` and job_name LIKE ?`;
    }

    if (context.parentJob) {
        values.push(context.parentJob);

        query += ` and super_parent_job = ?`;
    }

    query += ` order by run_date desc`;

    const result = await database.simpleExecute(query, values);

    var resObject = {};
    var test_trends = [];
    for (i = 0; i < result.length; i++) {

        if (i === 0) {
            resObject.super_parent_job = result[i].super_parent_job;
            resObject.job_name = result[i].job_name;
            resObject.name_of_test = result[i].name_of_test;
        }

        test_trends.push({
            status: result[i].status,
            date: result[i].rdate,
            bug: result[i].Bug_Id,
            issueType: result[i].bug_reason,
            comment: result[i].comment
        });

        resObject.test_trends = test_trends;
    }

    console.log(resObject);
    return resObject;
}

module.exports.find = find;